# emr-hive-jdbc-example

Example of how to connect to AWS EMR via Hive JDBC.

See my blog post for more details:  [Using Clojure to query AWS EMR via Hive JDBC](https://ejstembler.com/2018/09/using-clojure-to-query-aws-emr-via-hive-jdbc).

## Installation

Download from https://gitlab.com/ejstembler/emr-hive-jdbc-example.

## Usage

To run via [leiningen](https://leiningen.org):

    $ lein run

To run the packaged JAR:

    $ java -jar emr-hive-jdbc-example-0.1.0-standalone.jar

## License

The MIT License (MIT)
Copyright © 2018 Edward J. Stembler

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
