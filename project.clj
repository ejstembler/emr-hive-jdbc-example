(defproject emr-hive-jdbc-example "0.1.0-SNAPSHOT"
  :description "Example of how to connect to AWS EMR via Hive JDBC"
  :url "https://gitlab.com/ejstembler/emr-hive-jdbc-example"
  :license {:name "MIT"
            :url "https://mit-license.org"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.logging "0.4.1"]
                 [org.clojure/java.jdbc "0.7.8"]]
  :resource-paths ["resources/HiveJDBC41.jar"
                   "resources/commons-codec-1.3.jar"
                   "resources/commons-logging-1.1.1.jar"
                   "resources/hive_metastore.jar"
                   "resources/hive_service.jar"
                   "resources/httpclient-4.1.3.jar"
                   "resources/httpcore-4.1.3.jar"
                   "resources/libfb303-0.9.0.jar"
                   "resources/libthrift-0.9.0.jar"
                   "resources/log4j-1.2.14.jar"
                   "resources/ql.jar"
                   "resources/slf4j-api-1.5.11.jar"
                   "resources/slf4j-log4j12-1.5.11.jar"
                   "resources/TCLIServiceClient.jar"
                   "resources/zookeeper-3.4.6.jar"]
  :main ^:skip-aot emr-hive-jdbc-example.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
