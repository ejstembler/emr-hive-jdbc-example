(ns emr-hive-jdbc-example.core
  (:require [clojure.tools.logging :as log])
  (:require [clojure.java.jdbc :as jdbc])
  (:gen-class))

;; Define the user/password/subname from ENV variables
(def user (System/getenv "EMR_HIVE_JDBC_USER"))
(def password (System/getenv "EMR_HIVE_JDBC_PASSWORD"))
(def subname (System/getenv "EMR_HIVE_JDBC_SUBNAME"))

;; Preconditions
(when (nil? user)
  (throw (RuntimeException. "user is nil")))
(when (nil? password)
  (throw (RuntimeException. "password is nil")))
(when (nil? subname)
  (throw (RuntimeException. "subname is nil")))

;; Define the connection specification
(def hive-spec {:classname "com.amazon.hive.jdbc41.HS2Driver"}
               :subprotocol "hive2"
               :subname subname
               :user user
               :password password)

;; Define the SQL statement to execute
(def sql ["SELECT
               COUNT(*) AS row_count
           FROM
               my_table"]) ; TODO: Change to your desired SQL statement

;; The main entry-point
;; 1. Logs the SQL to execute
;; 2. Executes the SQL
;; 3. Returns the row_count value of the first row
(defn -main
  [& args]
  (log/info "Executing SQL:\n" (first sql))
  (println (jdbc/query hive-spec sql {:row-fn :row_count :result-set-fn first})))
